Proyecto Notas
Se solicita hacer un sistema de notas personales. El mismo debe implementarse con Bootstrap y Fontawesome. El proyecto es individual.
Cada nota tiene:
id (alfanumérico generado automáticamente de tamaño de 20 caracteres)
titulo (máximo 60 caracteres)
contenido (máximo 500 caracteres)
modificacion (fecha y hora de la última modificación)

Funcionalidades:
Agregar nota
Modificar de nota
Listar notas
Borrar nota
Buscar notas, por título y contenido

Debe implementarse la clase Nota. Las notas deben almacenarse en LocalStorage. Deben hacerse todas las validaciones correspondientes a los datos ingresados, tanto en el código HTML como en Javascript.

Para el proyecto debe usarse GIT. Crear un repositorio público en GitLab, luego del primer commit debe trabajarse sobre la rama develop, a medida que se vayan agregando funcionalidades se deben crear y publicar los commits correspondientes. Una vez finalizado el proyecto se debe mergear la rama master con la develop.

Requerimientos opcionales:
Agregar a Nota una categoría (máximo 30 caracteres).
En el alta de Nota, la categoría debe seleccionarse de una lista.
Se debe poder agregar y eliminar categorías.
Al eliminar una categoría debe verificarse que no exista una nota con dicha categoría asociada, es decir que sólo se puede borrar una categoría si no existen notas con dicha categoría.
Debe poder listar todas las notas de una categoría.

Fecha de presentación: 15/01/2020
